# Pristine - JS/CSS Library

> **Pristine has been updated and renamed to [pebble](https://gitlab.com/lucidlylogicole/pebble)**

Pristine is a web library with modern styles that can be utilized when you want. Pristine has fully styled elements, utility styles and useful JavaScript functions. The goal of Pristine is to improve efficiency on commonly used tasks, but leaving most of the customization to you.

Pristine can be used with normal web sites and web apps. Many styled elements do not require JavaScript, while some do (like tabs and page changes). Pristine also includes many convenient JavaScript functions like `$P.elm()` in replace of `document.getElementById()`.

This started as a collection of useful styles and JavaScript functions that I wanted to reuse. Included are some default styles that can be used for buttons and other elements. There are also some general utility styles for just adding a little css to an element (like a border radius or shadow).

---

**VERSION = 1.0.2**

## Documentation
- **[Documentation](https://lucidlylogicole.gitlab.io/pristine/docs/docs.html)** - view the docs
- **[Example Page](https://lucidlylogicole.gitlab.io/pristine/docs/ui_sample.html)** - View a bunch of styled sample elements

## Installation
1. Download **[pristine.css](https://gitlab.com/lucidlylogicole/pristine/raw/master/pristine.css)** for the styles
2. Download  **[pristine.js](https://gitlab.com/lucidlylogicole/pristine/raw/master/pristine.js)** to utilize the JavaScript functions
3. Include the css and js files
        
        <link href="pristine.css" rel="stylesheet" media="screen">
        <script src="pristine.js"></script>

## Requirements
- **Nothing** really. Pristine does not depend on jQuery or any other frameworks
- A **"modern" web browser** is likely needed if you want everything to work
    - Chrome and Firefox are fully supported, with attempts to make sure most things work with Safari and Edge.

## Philosophy
- Improve efficiency on commonly used tasks in web interface creation
- All Pristine features are optional (it will not override anything by default)
- Provide a simple, modern looking base style
- Allow for easy customization (don't fight Pristine to customize the interface)
- Simple functional JavaScript
- Provide most useful features instead of trying to do everything
- Don't break backwards compatibility of the API

## Updates
Pristine will minimize breaking backwards compatibility in future updates for documented features.  There is a chance some of the lower level or undocumented features could change (without affecting the main API).  That is one reason why they may be undocumented. For new developing features, the API may change until stable, but those features will be labeled as such.
