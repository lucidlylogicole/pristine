title> Dialogs
h> Dialogs
c> $P.dialog.message(options)
d> a message dialog (returns a promise resolving the output of the button text)
md> - options.msg - the message to display
- options.btns (list) - the list of buttons to display ['ok','cancel','yes','no'] (default is ok)
- options.title (string) - optional title for the message dialog
- options.close (bool) - optionally add a close button and close if user clicks off the dialog (default is false)
p> <button onclick="$P.dialog.message({msg:'a message box',btns:['ok','cancel']}).then(btn_text=>{})">message dialog 1</button> 
Message box with user defined buttons
pc> $P.dialog.message({msg:'a message box',
    btns:['ok','cancel']})
    .then(btn_text=>{})
p> <button onclick="$P.dialog.message({msg:'a message box',btns:['ok'],close:true,title:'A Message'})">message dialog 2</button>
message with title and close enabled
pc> $P.dialog.message({msg:'a message box',
    close:true,
    title:'A Message'
    btns:['ok']})
c> $P.dialog.close(element)
d> closes the dialog element (or all if left blank)
c> $P.dialog.show(element)
d> show a custom dialog
c> $P.dialog.input(options)
d> Text Input dialog (returns a promise resolving the value)
md> - options.msg - the message to display
- options.val - the default value
- options.title - optional titlebar text for dialog
p> <button onclick="$P.dialog.input({msg:'enter a value',val:'default'}).then(val=>{})">input dialog</button>
pc> $P.dialog.input({msg:'enter a value',val:'default'})
    .then(val=>{})

c> $P.dialog.list(options)
d> A list selection dialog  (returns a promise resolving the value)
md> - options.msg - the message to display
- options.val - the default value
- options.list - a list of values to choose from
- options.title - optional titlebar text for dialog
p> <button onclick="$P.dialog.list({title:'select a value',list:['a','b','c']}).then(val=>{})">list dialog</button>
pc> $P.dialog.list({title:'select a value',
    list:['a','b','c']})
    .then(val=>{})

h> Image Dialog
c> $P.dialog.image(options)
d> An image dialog
md> This can be used to show a larger image when it is clicked on
- **options.element** - specify an element to pull the image src from
- **options.src** - specify the src of the image

_only use **src** or **element** (element will override the src)_

ex> <img src="" onclick="$P.dialog.image({element:event.target})">

h> Dialog Classes
md> To manage your own dialog, you can utilize the following classes to aid with styling
c> .dlg-bg
d> the semi-transparent background behind the dialog
c> .dlg-frame
d> a centered frame for dialog content (small 350px)
c> .dlg-frame-wide
d> a centered frame for dialog content (wide 600px)
c> .dlg-frame-full
d> a centered expandable frame for dialog content
c> .dlg-title
d> top title bar of dialog
c> .dlg-text
d> side padded text inside the dialog

h> Custom Dialog Example
p> <div class="dlg-bg" id="mydialog">
    <div class="dlg-frame pad">
        <h1>My Custom Dialog</h1>
        <button class="btn" onclick="$P.dialog.close('mydialog')">
            ok</button>
    </div>
</div>
<button class="btn" onclick="$P.dialog.show('mydialog')">
    custom dialog</button>
pc> <div class="dlg-bg" id="mydialog">
    <div class="dlg-frame pad">
        <h1>My Custom Dialog</h1>
        <button class="btn" onclick="$P.dialog.close('mydialog')">
            ok</button>
    </div>
</div>
<button class="btn" onclick="$P.dialog.show('mydialog')">
    custom dialog</button>

<script>
    $P.dialog.show('mydialog') // show the dialog
    $P.dialog.close('mydialog') // hide the dialog
</script>