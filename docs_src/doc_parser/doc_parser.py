import os,sys, collections
import codecs, traceback

import commonmark

def parse(text=None,file=None):
    
    if file != None:
        with open(file,'r') as f:
            text = f.read()
    
    docD = {
        'title':'',
        'sections':[],
        'style':[],
        'js':[],
    }
    cur_header = -1  # Current Header index
    cur_content = -1 # Current Command index 
    cur_object = None  # Current Object index
    cur_command = -1
    line_count = 0
    
    
    for line in text.splitlines():
        line_count += 1
        linestrip = line.lstrip()
        if line.startswith('title>'):
            #---Title
            docD['title'] = line[6:].strip()
            cur_object = None
        elif line.startswith('style>'):
            docD['style'].append(line[6:].strip())
            cur_object = None
        elif line.startswith('js>'):
            docD['js'].append(line[3:].strip())
            cur_object = None
        elif line.startswith('h>'):
            #---Header
            hdr = line[2:].strip()
            cur_header += 1
            docD['sections'].append({'title':hdr,'content':[]})
            cur_object = None
            cur_command = -1
            cur_content = -1
            cur_preview = -1
        elif line.startswith('c>'):
            #---Command
            
            # Check for header
            if cur_header == -1:
                raise ValueError("Command (c>) specified before header (h>): line %s" %line_count)
            
            cmd = line[2:].strip()
            cur_content += 1
            cur_preview = -1
            cur_command = cur_content
            docD['sections'][cur_header]['content'].append({'typ':'cmd','cmd':cmd,'args':'','description':''})
            cur_object = None
        
        elif linestrip.startswith('ca>'):
            #---Command Extra content (not bold)
            
            # Check for command
            if cur_command == -1:
                raise ValueError("Command args (ca>) specified before command (c>): line %s" %line_count)
            
            docD['sections'][cur_header]['content'][cur_command]['args']= linestrip[3:].strip()
            
        elif linestrip.startswith('d>'):
            #---Command Description
            
            # Check for command
            if cur_command == -1:
                raise ValueError("Command description (d>) specified before command (c>): line %s" %line_count)
            
            docD['sections'][cur_header]['content'][cur_command]['description'] = linestrip[2:].strip()

        elif linestrip.startswith('p>'):
            # Check for header
            if cur_header == -1:
                raise ValueError("Preview (p>) specified before header (h>): line %s" %line_count)
            #---Preview
            cur_content += 1
            cur_preview = cur_content
            cur_object = 'p>'
            ln1 = line[2:].strip()
            docD['sections'][cur_header]['content'].append({'typ':'preview','text':ln1,'code':''})
            
        elif linestrip.startswith('pc>'):
            #---Preview Code
            if cur_preview == -1:
                raise ValueError("Preview Code (pc>) specified before preview (p>): line %s" %line_count)
            docD['sections'][cur_header]['content'][cur_preview]['code'] += linestrip[4:].rstrip()
            cur_object = 'pc>'
            
        elif linestrip.startswith(('note>','ex>','md>','html>','script>','n>')):
            cur_content += 1
            typ = linestrip.split('>')[0]
            docD['sections'][cur_header]['content'].append({'typ':typ,'text':linestrip[(len(typ)+2):].rstrip('\n')})
            cur_object = typ+'>'

        elif cur_object != None:
            if cur_content != -1:
                if cur_object == 'pc>':
                    # Append Code
                    docD['sections'][cur_header]['content'][cur_content]['code'] += '\n'+line.rstrip()
                else:
                    # Append to description
                    docD['sections'][cur_header]['content'][cur_content]['text'] += '\n'+line.rstrip()

    # Parse markdown
    parser = commonmark.Parser()
    renderer = commonmark.HtmlRenderer()
    for sec in docD['sections']:
        for content in sec['content']:
            if content['typ'] in ['notes','md']:
                content['text'] = renderer.render(parser.parse(content['text']))

    return docD

def toHtml(document,**kargs):
    
    template_path = os.path.join(os.path.dirname(__file__),'document_template.thtml')
    if 'template' in kargs and kargs['template'] != None:
        template_path = kargs['template']
    
    import pystache
    docD = {'sections':[],'table_of_contents':[]}
    
    for ky in document:
        print(ky)#,document[ky])
        if ky != 'sections':
            docD[ky]=document[ky]
    
    cnt = 0
    for section in document['sections']:
        cnt += 1
        secD = {'title':section['title'],'content':[],'sec_id':cnt}
        docD['table_of_contents'].append({'title':section['title'],'sec_id':cnt})
        for content in section['content']:
            
            contD = content
            contD['typ_'+content['typ']]=1
            if  content['typ'] in ['md','html']:
                contD['raw']=1
                contD['raw_class']='cls_'+content['typ']
            secD['content'].append(contD)
        docD['sections'].append(secD)
    
    txt = codecs.open(template_path,'r','utf-8').read()
    u_renderer = pystache.Renderer(string_encoding='utf8')
    html = u_renderer.render(txt,docD)
    return str(html)

#---Main
if __name__=='__main__':
    # Command Line Mode - Parse File
    if len(sys.argv) > 1:
        flnm = sys.argv[1]
        
        mode='text'
        if 'html' in sys.argv:
            mode = 'html'
        
        result = ''
        
        try:
            if mode == 'text':
                result = parse(file=flnm)
            elif mode == 'html':
                result = toHtml(parse(file=flnm))
        except:
            result = traceback.format_exc()
        
        # Compile
        if 'compile' in sys.argv:
            new_file = sys.argv[-1]
            with codecs.open(new_file,'w','utf-8') as file:
                file.write(result)
        else:
            print(result)