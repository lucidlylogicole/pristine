
//---o:
class $PGauge {
    constructor(options) {
        if (options === undefined){options = {}}
        
        // assign options
        for (var ky in options) {
            this[ky] = options[ky]
        }
        
        // Set min and max
        if (this.min === undefined) {this.min = 0}
        if (this.max === undefined) {this.max = 100}
        
    }
}

$P.gauges = {

//---Arc Calculation Functions
    polarToCartesian:function(centerX, centerY, radius, angleInDegrees) {
      var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
    
      return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
      };
    },

    describeArc:function(x, y, radius, startAngle, endAngle){
    
        var start = $P.gauges.polarToCartesian(x, y, radius, endAngle);
        var end = $P.gauges.polarToCartesian(x, y, radius, startAngle);
    
        var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";
    
        var d = [
            "M", start.x, start.y, 
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");
    
        return d;       
    },

//---Gauges
    //---o:
    radial: function(options) {
        class RadialGauge extends $PGauge {
            constructor(options) {
                super(options)
                if (this.start_angle === undefined) {this.start_angle = -120}
                if (this.end_angle === undefined) {this.end_angle = 120}
        
                this.r = 60
                this.w = 10
                if (options.radius !== undefined){this.r = options.radius}
                if (options.width !== undefined){this.w = options.width}
        
                // Construct svg
                this.elm = $P.create({
                    tag:'svg',
                    width:this.r*2,
                    height:this.r*2,
                    viewBox:"0 0 "+(this.r*2)+" "+(this.r*2),
                    class:'gauge-radial',
                    children:[
                        {tag:'path', class:"gauge-arc-val", fill:'none', 'stroke-width':this.w, 'stroke-opacity':0.8},
                        {tag:'path', class:"gauge-arc-base", fill:'none', 'stroke-width':this.w-4, 'stroke-opacity':0.3,
                            d:$P.gauges.describeArc(this.r, this.r+10, this.r-10, this.start_angle,this.end_angle)},
                        {tag:'path', class:"gauge-arc-rim", fill:'none', 'stroke-width':2, 'stroke-opacity':0.45,
                            d:$P.gauges.describeArc(this.r, this.r+10, this.r-4, this.start_angle,this.end_angle)},
                        {tag:'text', class:"gauge-radial-text", x:"50%", y:this.r+10, 'fill-opacity':0.8, 'font-size':this.r/2, 'dominant-baseline':'middle', 'text-anchor':'middle'},
                    ],
                    parent:this.parent
                },{svg:true})
        
                // Set Color
                if (this.color === undefined) {
                    this.color = 'rgb(158,19,19)'
                }
                this.setColor(this.color)
                
                // Setup Value
                if (this.val !== undefined) {
                    this.set(this.val)
                }
                
            }
            
            //---f:
            set(val) {
                this.val = val
                var ea = (this.val-this.min)/(this.max-this.min)*(this.end_angle-this.start_angle)+this.start_angle
                if (ea > this.end_angle) {ea = this.end_angle}
                else if (ea < this.start_angle) {ea = this.start_angle}
                $P.query(this.elm," .gauge-arc-val")[0].setAttribute("d", $P.gauges.describeArc(this.r, this.r+10, this.r-12, this.start_angle,ea))
                $P.query(this.elm,' .gauge-radial-text')[0].textContent = ''+val
            }
        
            //---f:
            setColor(clr) {
                this.color = clr
                $P.query(this.elm," .gauge-arc-base")[0].setAttribute("stroke",this.color)
                $P.query(this.elm," .gauge-arc-rim")[0].setAttribute("stroke",this.color)
                $P.query(this.elm," .gauge-arc-val")[0].setAttribute("stroke",this.color)
                $P.query(this.elm," .gauge-radial-text")[0].setAttribute("fill",this.color)
            }
        }
        return new RadialGauge(options)
    },
    
    //---o:
    bar: function(options) {
        class BarGauge extends $PGauge {
            constructor(options) {
                super(options)
                
                this.w = 20
                this.h = 100
                this.o = 10
                if (options.height !== undefined){this.h = options.height}
                if (options.width !== undefined){this.w = options.width}
                var o2 = this.o*2
                
                this.elm = $P.create({
                    tag:'svg',
                    width:this.w+o2,
                    height:this.h+o2,
                    viewBox:"0 0 "+(this.w+o2)+" "+(this.h+o2),
                    class:'gauge-rect',
                    children:[
                        {tag:'rect', class:"gauge-rect-base", width:this.w, height:this.h, x:this.o, y:this.o, 'fill-opacity':0.3, rx:2},
                        {tag:'rect', class:"gauge-rect-rim", fill:'none', width:this.w+4, height:this.h+4, x:8, y:8, 'fill-opacity':0.45, rx:2},
                        {tag:'rect', class:"gauge-rect-val", width:this.w, height:50, x:this.o, y:60, 'fill-opacity':0.8, rx:2},
                    ],
                    parent:this.parent
                },{svg:true})
        
                // Set Color
                if (this.color === undefined) {
                    this.color = 'rgb(158,19,19)'
                }
                this.setColor(this.color)
        
                // Setup Value
                if (this.val !== undefined) {
                    this.set(this.val)
                }
        
            }
            
            //---f:
            set(val) {
                this.val = val
                var h = (this.val-this.min)/(this.max-this.min)*this.h
                if (h > this.h) {h=this.h}
                else if (h < 0) {h=0}
                var y = this.h+this.o-h
                $P.query(this.elm," .gauge-rect-val")[0].setAttribute("height",h)
                $P.query(this.elm," .gauge-rect-val")[0].setAttribute("y",y)
            }
            
            //---f:
            setColor(clr) {
                this.color = clr
                $P.query(this.elm," .gauge-rect-base")[0].setAttribute("fill",this.color)
                $P.query(this.elm," .gauge-rect-rim")[0].setAttribute("stroke",this.color)
                $P.query(this.elm," .gauge-rect-val")[0].setAttribute("fill",this.color)
            }
        }
        return new BarGauge(options)
    },
    
    //---o:
    hbar: function(options) {
        class HBarGauge extends $PGauge {
            constructor(options) {
                super(options)
        
                this.w = 100
                this.h = 20
                this.o = 10
                if (options.height !== undefined){this.h = options.height}
                if (options.width !== undefined){this.w = options.width}
                var o2 = this.o*2
        
                this.elm = $P.create({
                    tag:'svg',
                    width:this.w+o2,
                    height:this.h+o2,
                    viewBox:"0 0 "+(this.w+o2)+" "+(this.h+o2),
                    class:'gauge-rect',
                    children:[
                        {tag:'rect', class:"gauge-rect-base", width:this.w, height:this.h, x:this.o, y:this.o, 'fill-opacity':0.3, rx:2},
                        {tag:'rect', class:"gauge-rect-rim", fill:'none', width:this.w+4, height:this.h+4, x:8, y:8, 'fill-opacity':0.45, rx:2},
                        {tag:'rect', class:"gauge-rect-val", width:50, height:this.h, x:this.o, y:this.o, 'fill-opacity':0.8, rx:2},
                    ],
                    parent:this.parent
                },{svg:true})
        
                // Set Color
                if (this.color === undefined) {
                    this.color = 'rgb(158,19,19)'
                }
                this.setColor(this.color)
        
                // Setup Value
                if (this.val !== undefined) {
                    this.set(this.val)
                }
        
            }
            
            //---f:
            set(val) {
                this.val = val
                var h = (this.val-this.min)/(this.max-this.min)*this.w
                if (h > this.w) {h=this.w}
                else if (h < 0) {h=0}
                var y = this.w+this.o-h
                $P.query(this.elm," .gauge-rect-val")[0].setAttribute("width",h)
            }
            
            //---f:
            setColor(clr) {
                this.color = clr
                $P.query(this.elm," .gauge-rect-base")[0].setAttribute("fill",this.color)
                $P.query(this.elm," .gauge-rect-rim")[0].setAttribute("stroke",this.color)
                $P.query(this.elm," .gauge-rect-val")[0].setAttribute("fill",this.color)
            }
        }
        return new HBarGauge(options)
    },
}