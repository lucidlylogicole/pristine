
//---$P.table
$P.table = class {
    constructor(options) {
        this.rowCount = 0
        this.colCount = 0
        this.contenteditable=0
        this.sortEnabled = 0
        this.sortOptions={lastColumn:0,lastDescending:1,number_sorting:1}
        this.startColumn = 0
        this.showRowHeader=0
        
        if (options !== undefined && options.element) {
            this.element = $P.elm(options.element)
        }
        if (!this.element) {
            this.element = $P.create({h:'table'})
        }
        this.element.$PTable = this
        
        // Generate ID of not
        if (!this.element.id) {
            this.element.id=$P.uuid(0)
        }
        
        // Build
        if (options !== undefined) {
            this.build(options)
        }
    }
    
    //---f:
    build(options) {
        for (let ky in options) {
            if (ky != 'element') {
                this[ky] = options[ky]
            }
        }
        
        if (this.rowCount+this.colCount > 0) {
            this.clear(true)
        }
        
        // Column Headings
        this.colCount = 0
        this.columnD = {}
        if (options.columns !== undefined) {
            let h_cols = []
            if (this.showRowHeader) {
                this.startColumn = 1
                h_cols.push({h:'th.tbl-row-count'})
            }
            for (let c of options.columns) {
                this.colCount++
                let cD = {
                    name:c,
                    visible:1,
                    // align:'l',
                }
                this.columnD[this.colCount] = cD
                
                // Add Table Sort To column onclick
                let onclk
                if (this.sortEnabled) {
                    onclk=`$P.elm('${this.element.id}').$PTable.toggleSort(${this.colCount-1},${this.sortOptions.number_sorting})`
                }
                
                h_cols.push({h:'th',ci:this.colCount ,text:cD.name, onclick:onclk})
            }
            // if (options.style) {
            //     $P.create({h:'style',text:options.style},{parent:this.element})
            // }
            this.thead = $P.create({h:'thead',c:[{h:'tr',c:h_cols}]},{parent:this.element})
            this.rowCount = 0
            this.tbody = $P.create({h:'tbody'},{parent:this.element})
            
            // Add Data
            this.addData(options.data)
        } else {
            // Try to load existing table
            this.tbody = $P.query(this.element,'tbody')[0]
            this.thead = $P.query(this.element,'thead')[0]
            
            // Load Table info
            for (let th of $P.query(this.thead,'th')) {
                this.colCount++
                this.columnD[this.colCount] = {name:th.innerText, visible:1}
            }
            this.rowCount=$P.query(this.tbody,'tr').length
        }
    }
    
    //---f:
    addData(data) {
        // Data
        let h_rows = []
        for (let rw of data) {
            this.rowCount ++
            let h_cells = []
            if (this.showRowHeader) {
                h_cells.push({h:'th.tbl-row-count',text:this.rowCount})
            }
            let ccnt = 0
            for (let col of rw) {
                ccnt ++
                h_cells.push({h:'td', ci:ccnt,text:col, contenteditable:this.contenteditable})
            }
            h_rows.push({h:'tr', ri:this.rowCount, c:h_cells})
        }
        
        $P.create(h_rows,{parent:this.tbody})
        
    }
    
    //---f:
    clear(all) {
        $P.clear($P.query(this.element,'tbody tr'))
        this.rowCount = 0
        
        if (all) {
            $P.clear($P.query(this.element,'thead, tbody'))
            $P.clear($P.query(this.element,'style'))
            this.rowCount = 0
        }
    }

//---Column
    //---f:
    insertColumn(options) {
        let opt = {
            text:'',
        }
        for (let ky in options) {
            opt[ky] = options[ky]
        }
        
        this.colCount++
        
        let ci = this.colCount-1
        let th_elm = $P.create({h:'th',ci:this.colCount ,text:opt.text})
        let thead_tr = this.thead.children[0]
        if (opt.before) {
            ci = opt.before
            thead_tr.insertBefore(th_elm,$P.query(this.element,`thead tr th[ci="${ci}"]`)[0])
        } else if (opt.after) {
            ci = opt.after
            thead_tr.insertBefore(th_elm,$P.query(this.element,`thead tr th[ci="${ci}"]`)[0].nextSibling)
        } else {
            thead_tr.appendChild(th_elm)
        }
        
        // Add to Rows
        let td_elms = $P.query(this.element,`tr td[ci="${ci}"]`)
        for (let td of td_elms) {
            // td.style.color='red'
            
            let new_col = $P.create({h:'td', ci:this.colCount})
            
            if (opt.before) {
                // before row
                td.parentElement.insertBefore(new_col,td)
            } else if (opt.after) {
                // after row
                td.parentElement.insertBefore(new_col,td.nextSibling)
            } else {
                // at end
                td.parentElement.appendChild(new_col)
            }
            
        }
        return this.colCount
    }
    
    //---f:
    showColumn(col_ind,show) {
        if (show || show===undefined) {
            $P.class.remove($P.query(this.element,`[ci="${col_ind}"]`),'hidden')
        } else {
            $P.class.add($P.query(this.element,`[ci="${col_ind}"]`),'hidden')
        }
    }

    
//---Rows
    //---f:
    insertRow(options) {
        let data = []
        if (!options) {
            options = {}
        }
        if (options.data) {
            data = options.data
        }
        
        this.rowCount++
        let ri = this.rowCount
        
        let h_cells = [{h:'th.tbl-row-count',text:this.rowCount}]
        for (let c=0; c <this.colCount; c++) {
            let txt = ''
            if (data.length > c) {
                txt = data[c]
            }
            h_cells.push({h:'td', ci:c, text:txt, contenteditable:this.contenteditable})
        }
        
        let new_rw = $P.create({h:'tr', ri:ri, c:h_cells})
        
        if (options.before) {
            // before row
            this.element.insertBefore(new_rw,this.row(options.before))
        } else if (options.after) {
            // after row
            this.element.insertBefore(new_rw,this.row(options.after).nextSibling)
        } else {
            // at end
            this.element.appendChild(new_rw)
        }
        
        return this.rowCount
    }

    //---f:
    row(r) {
        return $P.query(this.element,`tr[ri="${r}"]`)[0]
    }

//---Data
    //---f:
    cell(row,col) {
        return new Cell($P.query(this.element,`tr[ri="${row}"] td[ci="${col}"]`)[0])
    }
    
    //---f:
    getData(options) {
        let opt = {
            header:1,
            type:'object', // object,csv
        }
        
        for (let ky in options) {
            opt[ky] = options[ky]
        }
        
        
        // Header
        let cols
        if (opt.header) {
            cols = []
            for (let col of Array.prototype.slice.call($P.query(this.element,'thead th'),this.startColumn)) {
                cols.push(col.innerText)
            }
        }
        
        // Rows
        let data = []
        for (let tr of $P.query(this.tbody,'tr')) {
            let rw = []
            for (let td of $P.query(tr,'td')) {
                rw.push(td.innerText)
            }
            data.push(rw)
        }
        
        // Return
        if (opt.type == 'object') {
            return {columns:cols, data:data}
        } else if (opt.type == 'csv' || opt.type=='text') {
            let delim='\t'
            if (opt.type == 'csv') {
                delim = ','
            }
            
            let txt = ''
            if (cols) {
                txt += cols.join(delim)
            }
            
            for (let rw of data) {
                if (txt != '') {txt += '\n'}
                txt += rw.join(delim)
            }
            
            return txt
            
        } else {
            return data
        }
    }
    
//---Misc
    //---f:
    search(txt,cols) {
        let hide_elms = []
        let show_elms=[]

        if (txt == '') {
            show_elms = $P.query(this.tbody,'tr')
        } else {
            for (let tr of $P.query(this.tbody,'tr')) {
                let show_row = 0
                let tds = $P.query(tr,'td')
                for (let c=0; c<tds.length; c++) {
                    if (cols === undefined || cols.indexOf(c)>-1) {
                        if (tds[c].innerText.toLowerCase().indexOf(txt.toLowerCase()) > -1) {
                            show_row = 1
                            break
                        }
                    }
                }
                if (show_row) {
                    show_elms.push(tr)
                } else {
                    hide_elms.push(tr)
                }
            }
        }
        $P.class.add(hide_elms,'hidden')
        $P.class.remove(show_elms,'hidden')
    }
    //---f:
    sort(col, descending, sort_by_number, sort_function) {
        let tbl_rows = $P.query(this.tbody,'tr')
        let rows_array = []
        for(let r=0; r<tbl_rows.length; r++) {
            let col_elm = $P.query(tbl_rows[r],'td')[col]
            rows_array[r]={old_index:r, val:col_elm.innerText, elm:tbl_rows[r]}
            // If sort by number
            if (sort_by_number) {
                let v = parseFloat(rows_array[r].val)
                if (!isNaN(v)){rows_array[r].val = v}
            }
        }
        
        // Sort the Array
        if (sort_function !== undefined) {
            // User Defined Sort Function
            rows_array.sort(sort_function)
        } else {
            rows_array.sort((a,b)=>{
                return(a.val == b.val ? 0 : (a.val > b.val ? 1 : -1))
            })
        }
        
        // Check Descending
        if (descending) {rows_array.reverse()}
        
        // Create new table body and replace
        let new_tbody = $P.create({h:'tbody'})
        for(let r=0; r<rows_array.length; r++) {
            new_tbody.appendChild(rows_array[r].elm)
        }
        let old_tbody = this.tbody
        this.element.replaceChild(new_tbody,old_tbody)
        this.tbody = new_tbody
    }
    
    //---f:
    toggleSort(col, sort_by_number, sort_function) {
        let desc = 0
        if (col == this.sortOptions.lastColumn) {
            desc = !this.sortOptions.lastDescending
        }
        this.sort(col,desc,sort_by_number, sort_function)
        this.sortOptions.lastColumn = col
        this.sortOptions.lastDescending = desc
    }
    
}

class Cell {
    constructor(elm) {
        this.element = elm
    }
    
    //---val
    get val() {
        return this.element.innerText
    }
    
    set val(newval) {
        this.element.innerText = newval
    }
}

// $P.ui.table = $PTable
