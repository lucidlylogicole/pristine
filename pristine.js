// Version 1.0.3
//---Window Onload

window.addEventListener('load',function(event) {
    // Hash Change Listener event
    if ($P.options.hashchange) {
        window.addEventListener("hashchange", $P.loc.hash.change, false)
        $P.loc.hash.change()
    }
    
    // Load Default Active Pages
    if (!location.hash || !$P.options.hashchange) {
        var pgs = document.querySelectorAll('.page.active')
        for (var i=0; i < pgs.length; i++) {
            $P.show(pgs[i],nohash=1)
        }
    }
})


//---
//---$P
var $P = {
    options: {
        hashchange:false,
        pages:true,
    },

    //---Element Functions
    elm: function(elm_or_id) {
        if (typeof elm_or_id == 'string') {
            return document.getElementById(elm_or_id)
        } else {
            return elm_or_id
        }
    },

    iterElements: function(elm_or_id_or_arr, func) {
        if (elm_or_id_or_arr.constructor === Array || elm_or_id_or_arr.constructor === NodeList) {
            for (var p=0; p < elm_or_id_or_arr.length; p++) {
                func($P.elm(elm_or_id_or_arr[p]))
            }
        } else {
            func($P.elm(elm_or_id_or_arr))
        }
    },

    query: function(elm_or_id,qry) {
        var elm = document
        if (qry !== undefined) {
            elm = $P.elm(elm_or_id)
        } else {
            qry = elm_or_id
        }
        return elm.querySelectorAll(qry)
    },

    parent: function(elm_or_id) {
        return this.elm(elm_or_id).parentElement
    },
    
    append: function(elm_or_id,chidren) {
        var par_elm = this.elm(elm_or_id)
        $P.iterElements(chidren, function(elm){
            par_elm.appendChild(elm)
        })
    },
    
    remove: function(elm_or_id) {
        $P.iterElements(elm_or_id, function(elm){
            if (elm !== undefined && elm.parentNode != undefined) {
                elm.parentNode.removeChild(elm)
            }
        })
    },

    clear: function(elm_or_id) {
        $P.iterElements(elm_or_id, function(elm){
            while (elm.firstChild) {
                elm.removeChild(elm.firstChild)
            }
        })
    },

    exists: function(elm_or_id) {
        return $P.elm(elm_or_id) !== null
    },

    //---Create
    create: function(elements,options) {
        
        if (elements.constructor !== Array) {
            elements = [elements]
        }
        
        if (options === undefined) {
            options = {}
        }
        var element_list = []
        for (var e=0; e < elements.length; e++) {
            var element = elements[e]
            var tag,attr={},parent
            
            for (var ky in element) {
                if (ky == 'tag') {
                    tag = element.tag
                } else if (ky == 'parent') {
                    parent = element.parent
                } else if (ky == 'h') {
                    // Hypertext like
                    var cl_spl = element.h.split('.')
                    var id_spl = cl_spl[0].split('#')
                    var tg_spl = id_spl[0].trim()
                    if (tg_spl) {
                        element.tag = tg_spl
                    }
                    if (id_spl.length > 1) {
                        attr.id = id_spl[1].trim()
                    }
                    if (cl_spl.length > 1) {
                        attr.class = cl_spl.slice(1).join('.')
                    }
                } else if (ky == 'c') {
                    // Replace c key with children
                    delete Object.assign(element, {'children': element['c']}).c 
                } else if (ky == 'children') {
                    // ignore for now
                } else if (ky == 'e' || ky == 'events') {
                    // ignore events
                    if (ky == 'e') {
                        delete Object.assign(element, {'events': element['e']}).e
                    }
                } else if (element[ky] !== undefined) {
                    attr[ky] = element[ky]
                }
            }
            
            // Create Element
            if (element.tag === undefined || element.tag === null) {
                element.tag='div'
            }
            
            // Element
            var elm
            if (options.svg) {
                elm = document.createElementNS("http://www.w3.org/2000/svg", element.tag)
            } else {
                elm = document.createElement(element.tag)
            }
            
            // Add Attributes
            for (var ky in attr) {
                if (ky == 'innerHTML') {
                    elm.innerHTML += attr.innerHTML
                } else if (ky == 'text') {
                    elm.innerHTML += attr.text
                } else if (ky == 'innerText') {
                    elm.innerText += attr.text
                } else {
                    elm.setAttribute(ky,attr[ky])
                }
            }

            // Add Event Listeners
            if ('events' in element) {
                for (let eky in element.events) {
                    elm.addEventListener(eky,element.events[eky])
                }
            }

            // Add Children
            if ('children' in element && element['children'] != undefined) {
                for (var c=0; c < element.children.length; c++) {
                    if (element.children[c] instanceof Element) {
                        elm.appendChild(element.children[c])
                    } else {
                        element.children[c].parent = elm
                        this.create([element.children[c]],options)
                    }
                }
            }
            

            
            element_list.push(elm)
            
            // Add to Parent
            var par_elm
            if (element.parent !== undefined && element.parent !== null) {
                par_elm = element.parent
            } else if (options.parent !== undefined) {
                par_elm = options.parent
            }
            if (par_elm !== undefined && par_elm !== null) {
                $P.elm(par_elm).appendChild(elm)
            }
        }
        
        if (element_list.length == 1) {
            return element_list[0]
        } else {
            return element_list
        }
        
    },

    //---Element Attributes
    attr: function(elm_or_id,atrb,val) {
        var elm = this.elm(elm_or_id)
        if ($P.isObject(atrb)) {
            // Object, set multiple attributes
            for (var ky in atrb) {
                elm.setAttribute(ky,atrb[ky])
            }
        } else if (val === undefined) {
            // Get
            return elm.getAttribute(atrb)
        } else {
            // Set
            elm.setAttribute(atrb,val)
            return elm
        }
    },
    
    html: function(elm_or_id,txt) {
        if (txt === undefined) {
            return this.elm(elm_or_id).innerHTML
        } else {
            $P.iterElements(elm_or_id, function(elm){
                elm.innerHTML=txt
            })
        }
    },
    
    val: function(elm_or_id,txt) {
        var main_elm = this.elm(elm_or_id)
        if (['input','select','textarea'].indexOf(main_elm.tagName.toLowerCase()) > -1) {
            // Single Input
            if (txt === undefined) {
                // Return Value
                var v = main_elm.value
                if (main_elm.type == 'checkbox') {
                    v = main_elm.checked
                } else if (main_elm.type == 'radio') {
                    v = main_elm.checked
                }
                return v
            } else {
                // Set Value
                if (main_elm.type == 'checkbox' || main_elm.type == 'radio') {
                    main_elm.checked = txt
                } else {
                    main_elm.value = txt
                }
            }
        } else {
            // Return Multiple values
            var valD = {}
            var in_elms = $P.query(elm_or_id,'input,textarea,select')
            for (var i=0; i < in_elms.length; i++) {
                var in_elm = in_elms[i]
                if ($P.attr(in_elm,'key') !== null) {
                    valD[$P.attr(in_elm,'key')]=$P.val(in_elm)
                } else if (in_elm.type == 'radio') {
                    if (in_elm.checked) {
                        valD[in_elm.name]=in_elm.value
                    }
                } else if (in_elm.id != '' && ['button','submit','reset'].indexOf((''+in_elm.type).toLowerCase()) <0) {
                    valD[in_elm.id]=$P.val(in_elm)
                }
            }
            return valD
            //---todo:set multiple values
        }
    },

    //---Display
    toggle: function(elm_or_id,force) {
        $P.iterElements(elm_or_id, function(elm){
            if (force) {
                if (getComputedStyle(elm,null).display !== 'none') {
                    $P.hide(elm,force)
                } else {
                    $P.show(elm,force)
                }
            } else {
                if ($P.class.has(elm,'hidden')) {
                    $P.show(elm)
                } else {
                    $P.hide(elm)
                }
            }
        })
    },

    show: function(elm_or_id,force) {
        $P.class.remove(elm_or_id,'hidden')
        
        if (force) {
            
            $P.iterElements(elm_or_id, function(elm){
                
                if (getComputedStyle(elm,null).display == 'none') {
                    if (elm.hasAttribute('data-display')) {
                        elm.style.display = $P.attr(elm,'data-display')
                    } else if(!$P.isVisible(elm)) {
                        elm.style.display = 'block'
                    }
                }
            })
        }
    },
    
    hide: function(elm_or_id,force) {
        $P.class.add(elm_or_id,'hidden')
        if (force) {
            $P.iterElements(elm_or_id, function(elm){
                if (elm.style.display != '' && elm.style.display !== undefined) {
                    elm.style.display = ''
                }
                $P.class.remove(elm,'active')
            })
        }
    },
    
    isVisible: function(elm_or_id) {
        var elm = $P.elm(elm_or_id)
        return !!( elm.offsetWidth || elm.offsetHeight || elm.getClientRects().length )
    },

//---Class Functions
    //---o:
    class: {
        add: function(elm_or_id,cls) {
            $P.iterElements(elm_or_id,function(elm){
                try {
                    elm.classList.add(cls)  // future
                } catch(e) {
                    elm.className += ' '+cls
                }
            })
        },
        
        has: function(elm_or_id,cls) {
            return $P.elm(elm_or_id).classList.contains(cls)
        },
        
        set: function(elm_or_id,cls) {
            $P.iterElements(elm_or_id,function(elm){
                elm.className = cls
            })
        },
        
        toggle: function(elm_or_id,cls) {
            $P.iterElements(elm_or_id,function(elm){
                if ($P.class.has(elm,cls)) {
                    $P.class.remove(elm,cls)
                } else {
                    $P.class.add(elm,cls)
                }
            })
        },
        
        remove: function(elm_or_id,cls) {
            var re = new RegExp(cls,"g")
            $P.iterElements(elm_or_id,function(elm){
                try {
                    elm.classList.remove(cls) 
                } catch(e) {
                    elm.className.replace(new RegExp('(?:^|\\s)'+ cls + '(?:\\s|$)'), ' ')
                }
            })
        },
    },

    //---Events
    listen: function(elm_or_id,event,func) {
        this.elm(elm_or_id).addEventListener(event, func)
        return this.elm(elm_or_id)
    },

    sendEvent: function(elm_or_id, event_name, detail, options) {
        if (options === undefined) {options = {}}
        options.detail = detail
        var evt = new CustomEvent(event_name,options)
        this.elm(elm_or_id).dispatchEvent(evt)
        return evt
    },

    promise: function() {
        // Get a promise to use resolve (accept) outside of promise
        var _resolve, _reject
        var promise = new Promise(function(resolve,reject){
            _resolve = resolve
            _reject = reject
        })
        
        promise.accept = function(value) {
            _resolve(value)
        }
        promise.cancel = function(value) {
            _reject(value)
        }
        
        return promise
    },

//---
//---Pristine Objects
    //---o:
    dialog: {
        // function(msg, btns, title, options) {
        open: function(options) {
            
            dflt_options = {
                close:false,
                fullscreen:false,
                wide:false,
            }
            
            options = $P.merge(dflt_options,options)
            
            // Title
            let title_elm = {}
            if (options.title !== undefined) {
                title_elm = {class:"dlg-title",text:options.title}
            } else if (options.msg !== undefined) {
                // Spacer
                title_elm = {class:"dlg-spacer",text:'&nbsp;'}
            }
            
            // Message
            let msg_elm
            if (typeof options.content == 'string') {
                msg_elm = {text:options.content}
            } else {
                msg_elm = options.content
            }
            
            // Bottom
            let btm_elm = {}
            if (options.btn_elms) {
                btm_elm = {class:"dlg-bottom",
                    children:options.btn_elms,
                }
            }
            
            // Dialog Elements
            var dlgElements = [{
                class:'dlg-bg-temp',
                children:[
                    {class:'dlg-frame',
                        children:[
                            title_elm,
                            msg_elm,
                            btm_elm,
                        ],
                    },
                ]
            }]
            
            // Add FullScreen
            if (options.frame_class != undefined) {
                dlgElements[0].children[0].class = options.frame_class
            } else if (options.fullscreen) {
                dlgElements[0].children[0].class = 'dlg-frame-full'
            } else if (options.wide) {
                dlgElements[0].children[0].class = 'dlg-frame-wide'
            }
            
            // Close Button
            if (options.close) {
                dlgElements[0].onclick = '(function (evt){if (evt.target.className=="dlg-bg-temp"){$P.dialog.close(event.target)}})(event)'
                dlgElements[0].children[0].children.unshift(
                    {tag:'span',
                        class:'btn-close',
                        onclick:'$P.dialog.close(event.target)',
                        title:'close',
                        text:'&times;',
                    }
                )
            }
            var dlg_elm = $P.create(dlgElements)
            document.body.appendChild(dlg_elm)
            
            return dlg_elm
        
        },
        
        show: function(elm_or_id) {
            let dlg_elm = $P.elm(elm_or_id)
            $P.class.add(dlg_elm,'active')
            $P.class.remove(dlg_elm,'hidden')
        },
        
        close: function(current_dialog) {
            let bkgd_elm = $P.elm(current_dialog)
            if (current_dialog!== undefined && $P.class.has(bkgd_elm,'dlg-bg')) {
                $P.class.remove(bkgd_elm,'active')
                return
            }
            if (current_dialog !== undefined) {
                let bkgd = bkgd_elm.closest(".dlg-bg-temp")
                $P.remove(bkgd)
            } else {
                // Close All
                var elems = document.getElementsByClassName("dlg-bg-temp")
                for (var i = 0; i < elems.length; i++) {
                    var elem = elems[i]
                    elem.parentNode.removeChild(elem)
                }
            }
        },
    
        // function(msg,btns,title,options) {
        message: function(options) {
            if (options === undefined) {
                options = {}
            }
            if (options.btns === undefined) {
                options.btns = ['ok']
                options.close = true
            }
            
            var dlg_doneP = $P.promise()
            dlg_doneP.close = function() {
                this.accept()
                $P.dialog.close()
            }
            var btn_elements = []
            for (var i=0; i < options.btns.length; i++) {
                btn_elements.push($P.create({tag:'button',class:'btn',text:options.btns[i]}))
                btn_elements[i].onclick = function(event){
                    
                    // Check for beforeClose function
                    var ok_close = 1
                    if (options.beforeClose !== undefined) {
                        ok_close = options.beforeClose(event.target.textContent)
                    }
                    if (ok_close) {
                        dlg_doneP.accept(event.target.textContent)
                        $P.dialog.close(event.target)
                    }
                }
            }
            
            options.content = options.msg
            if (typeof options.msg == 'string') {
                options.content = {class:"dlg-text",text:options.msg}
            }
            options.btn_elms = btn_elements
            $P.dialog.open(options)

            // Set focus to first button
            if (btn_elements.length > 0) {
                btn_elements[0].focus()
            }

            return dlg_doneP
        },
        
        input: function(options) {
            var val = ''
            if (options.val !== undefined) {val=options.val}
            if (options.msg === undefined) {options.msg=''}
            
            let dlg_id = $P.uuid(0)
            
            options.content = {class:'dlg-text',text:options.msg+'<br>',children:[{
                tag:'input',id:"i_"+dlg_id,type:"text",class:"pure wide", 
                onkeydown:"if (event.keyCode == 13) {event.preventDefault();document.getElementById('b_ok_"+dlg_id+"').click();}",
                style:"margin-top:10px;font-size:1.1rem;",value:val
            }]}
            
            var dlg_doneP = $P.promise()
            
            options.btn_elms = []
            // Ok 
            options.btn_elms.push($P.create({h:'button#b_ok_'+dlg_id+'.btn btn-dlg-ok',text:'ok'}))
            options.btn_elms[0].onclick = function(){
                dlg_doneP.accept(document.getElementById("i_"+dlg_id).value)
                $P.dialog.close()
            }
            // Cancel
            options.btn_elms.push($P.create({tag:'button',text:'cancel',class:'btn'}))
            options.btn_elms[1].onclick = function(event){
                dlg_doneP.accept()
                $P.dialog.close(event.target)
            }
            $P.dialog.open(options)
            // Focus and Set Cursor to end
            $P.elm("i_"+dlg_id).focus()
            $P.val("i_"+dlg_id,'')
            $P.val("i_"+dlg_id,val)
            
            return dlg_doneP
            
        },

        // List Select
        list: function(options) {
            options.content = {children:[]}
            
            if (options.msg !== undefined && options.msg != '') {
                options.content.children.push({class:'dlg-text pad-b',text:options.msg})
            }
            
            var dlg_doneP = $P.promise()
            var list_elm = {h:'.li-item-container ',children:[]}
            
            for (var i=0; i < options.list.length; i++) {
                
                var val = options.list[i]
                var txt = options.list[i]
                if (Array.isArray(options.list[i]) && options.list[i].length > 1) {
                    val = options.list[i][1]
                    txt = options.list[i][0]
                }
                
                list_elm.children.push({h:'.li-item-h sel',val:val,text:txt})
            }
            
            // Add list and setup onlick
            options.content.children.push($P.create(list_elm))
            options.content.children.slice(-1)[0].onclick = function(event) {
                dlg_doneP.accept(event.target.getAttribute('val'))
                $P.dialog.close(event.target)
            }
            
            options.btn_elms = []
            // Cancel
            options.btn_elms.push($P.create({tag:'button',text:'cancel',class:'btn'}))
            options.btn_elms[0].onclick = function(event){
                dlg_doneP.accept()
                $P.dialog.close(event.target)
            }

            $P.dialog.open(options)
            
            return dlg_doneP
        },
        
        image: function(options) {
            var dlg_doneP = $P.promise()
            dlg_doneP.close = function() {
                this.accept()
                $P.dialog.close()
            }
            
            options.frame_class='dlg-frame-none'
            options.close=true
            options.content = {tag:'img',class:"dlg-img",src:options.src}
            if (options.element !== undefined) {
                options.content = {tag:'img',class:"dlg-img",src:$P.attr(options.element,'src')}
            }

            dlg_elm = $P.dialog.open(options)
            return dlg_doneP
        }
        
    },


    //---Pages and Tabs
    //---o:
    loc: {
        
        //---o:
        hash: {
            CHANGE_ENABLED:1, // Temporary enablement
            
            change:function() {
                if ($P.loc.hash.CHANGE_ENABLED) {
                    if (!location.hash) {
                        // Show Defaults
                        var pgs = document.querySelectorAll('.page.default')
                        for (var i=0; i < pgs.length; i++) {
                            $P.page._show(pgs[i].id,nohash=1)
                        }
            
                    } else {
                        $P.page._show(location.hash.slice(1), nohash=1)
                    }
                }
            },
        },
        
        //---o:
        search: {
            args: function() {
                // Parse hash args
                let hD = {}
                
                if (location.search) {
                    location.search.slice(1).split('&').forEach(function (h){
                        var hspl = h.split('=')
                        hD[hspl[0]]=undefined
                        if (hspl.length > 1){
                            hD[hspl[0]] = hspl[1]
                        }
                    })
                }
                return hD
            },
            set: function(args,options) {
                if (options === undefined) {options = {}}
                let h = ''
                for (var ky in args) {
                    if (h != '') {h += '&'}
                    h += ky
                    if (args[ky]) {
                        h += '='+args[ky]
                    }
                }

                let title = document.title
                if (options.title) {title=options.title}
                
                let u = location.origin + location.pathname +'?'+ h + location.hash
                if (options.no_history == 1) {
                    window.history.replaceState('data', title, u)
                } else {
                    window.history.pushState('data', title, u)
                }
            },
            update: function(update_args,options) {
                let args = $P.loc.search.args()
                for (var ky in update_args) {
                    args[ky] = update_args[ky]
                }
                $P.loc.search.set(args,options)
            },
            remove: function(hash_arg,options) {
                let args = $P.loc.search.args()
                delete args[hash_arg]
                $P.loc.search.set(args,options)
            },
        },
    },
    
    //---o:
    page: {
        show: function(elm_or_id, nohash) {
            if (!nohash && $P.options.hashchange) {
                window.location.hash='#'+$P.elm(elm_or_id).id
            } else {
                $P.page._show(elm_or_id)
            }
        },
        _show: function(elm_or_id,nohash) {
            var pg_elm = $P.elm(elm_or_id)
            var found = 0
            if (pg_elm) {
                
                // Show Each Element (from below)
                function __show(elm) {
                    if (elm.id == pg_elm.id) {
                        $P.show(elm)
                        $P.class.add(elm,'active')
                        elm.scrollIntoView(true)
                        
                        // Add Top Spacing Scolling
                        var top_spc = document.querySelector('.header-spacer')
                        if (top_spc) {
                            window.scrollBy(0,-top_spc.offsetHeight)
                        }
                        
                        found = 1
                    } else {
                        $P.class.remove(elm,'active')
                    }
                }
                
                // Query each element and show
                try {
                    pg_elm.parentElement.querySelectorAll(':scope > .page').forEach(__show)
                } catch (e) {
                    // Since Edge can't fix bugs
                    pg_elm.parentElement.querySelectorAll('.page').forEach(__show)
                }
            }
            if (found && ! nohash) {
                window.location.hash='#'+pg_elm.id
            }
        },
    },

    //---o:
    tab: {
        click: function(event,options) {
            
            if (options === undefined) {options = {}}
            
            // Close Button Clicked
            if ($P.class.has(event.target,'btn-close')) {
                var tab_bar = event.target.parentElement.parentElement
                var ind = $P.attr(event.target.parentElement,'tab-index')
                
                var ok = 1
                if (options.beforeClose) {
                    ok = options.beforeClose(event.target.parentElement)
                }
                
                if (ok) {
                    $P.tab.close(event.target.parentElement)
                    
                    // Select another tab
                    var find_ind = 1
                    var cur_tab = tab_bar.querySelectorAll(':scope > .tab.active')
                    if (cur_tab.length > 0) {
                        if ($P.attr(cur_tab[0],'tab-index') != ind) {
                            cur_tab[0].click()
                            find_ind = 0
                        }
                    }
                    
                    if (find_ind) {
                        // Select nearest tab if current one not valid
                        var tabs = tab_bar.querySelectorAll(':scope > .tab')
                        if (tabs.length > 0) {
                            var new_ind = Math.min(tabs.length-1,ind-1)
                            new_ind = Math.max(0,new_ind)
                            tabs[new_ind].click()
                        }
                    }
                    
                    // Call afterClose callback
                    if (options.afterClose) {
                        options.afterClose()
                    }
                    
                    return
                }
            }
            
            // Find Tab
            var tab_elm = event.target
            if ($P.class.has(event.target.parentElement,'tab')) {
                tab_elm = event.target.parentElement
            }

            // Check for actual tab 
            if (!($P.class.has(tab_elm,'tab'))) {
                return
            }
            
            $P.tab.show(tab_elm, options)
            
        },
        show: function(tab_elm, options) {
            
            if (options === undefined) {options = {}}
            
            var tab_elm = $P.elm(tab_elm)
            var ind = tab_elm.getAttribute('tab-index') 
            var tab_cntr = tab_elm.parentElement.parentElement
            
            // Page Visibility
            var pages, sel_pages
            try {
                pages = tab_cntr.querySelectorAll(':scope > .tab-pages')[0].querySelectorAll(':scope > .tab-page')
                sel_pages = tab_cntr.querySelectorAll(':scope > .tab-pages')[0].querySelectorAll(':scope > .tab-page[tab-index="'+ind+'"]')
            } catch(e) {
                pages = tab_cntr.querySelectorAll('.tab-page')
                sel_pages = tab_cntr.querySelectorAll('.tab-page[tab-index="'+ind+'"]')
            }

            // Hide Pages
            for (var i = 0; i < pages.length; i++) {
                pages[i].classList.remove("active")
            }

            // Show Page
            if (sel_pages.length > 0) {
                for (var i = 0; i < sel_pages.length; i++) {
                    sel_pages[i].classList.add("active")
                }
            }
            
            // Tab Visibility
            var tabs, sel_tabs
            try {
                tabs = tab_cntr.querySelectorAll(':scope > .tab-bar')[0].querySelectorAll(':scope > .tab')
                sel_tabs = tab_cntr.querySelectorAll(':scope > .tab-bar')[0].querySelectorAll(':scope > .tab[tab-index="'+ind+'"]')
            } catch(e) {
                tabs = tab_cntr.querySelectorAll('.tab')
                sel_tabs = tab_cntr.querySelectorAll('.tab[tab-index="'+ind+'"]')
            }
            
            if (sel_tabs.length > 0) {
            
                // Hide Pages
                for (var i = 0; i < tabs.length; i++) {
                    tabs[i].classList.remove("active")
                }
                // Show Page
                for (var i = 0; i < sel_tabs.length; i++) {
                    sel_tabs[i].classList.add("active")
                }
            }
            
            // Tab Change
            if (options.tabChange) {
                options.tabChange(ind)
            }

        },
        close: function(tab_elm) {
            var tab_elm = $P.elm(tab_elm)
            var ind = tab_elm.getAttribute('tab-index') 
            var tab_cntr = tab_elm.parentElement.parentElement
            
            // Remove Page
            var sel_pages
            try {
                sel_pages = tab_cntr.querySelectorAll(':scope > .tab-pages')[0].querySelectorAll(':scope > .tab-page[tab-index="'+ind+'"]')
            } catch(e) {
                sel_pages = tab_cntr.querySelectorAll('.tab-page[tab-index="'+ind+'"]')
            }
            
            if (sel_pages.length > 0) {
                $P.remove(sel_pages[0])
            }
            
            delete sel_pages
            
            // Remove Tab
            $P.remove(tab_elm)
            delete tab_elm
            
        },
    },

    //---o:
    panel: {
        show: function(elm_or_id) {
            $P.class.add(elm_or_id,'active')
        },
        close: function(elm_or_id) {
            $P.class.remove(elm_or_id,'active')
        }
    },

    //---o:
    menu: {
        show: function(elm_or_id,options) {
            
            let opt = {x_off:0,y_off:0,fixed:0,}
            if (options !== undefined) {opt = $P.merge(opt,options)}
            
            // Show Menu Element
            var elm = $P.elm(elm_or_id)
            // elm.style.display='block'
            
            $P.class.add(elm,'active')
            
            // if (!opt.fixed) {
            //     opt.y_off += window.scrollY
            //     opt.x_off += window.scrollX
            //     elm.style.position='absolute'
            // } else {
            //     elm.style.position='fixed'
            // }

            let x = {}
            if (opt.anchor) {
                // Adjust Position to anchor element
                let anchor_elm = $P.elm(opt.anchor)
                let rect = anchor_elm.getBoundingClientRect()
                if (opt.loc == 'side') {
                    // Side Position
                    if (opt.align == 'left') {
                        x.left = rect.left-elm.offsetWidth+opt.x_off
                    } else {
                        x.left = rect.right+opt.x_off
                    }
                    x.top = rect.top+opt.y_off

                } else {
                    // Below Position
                    if (opt.align == 'right') {
                        x.left = rect.right-elm.offsetWidth+opt.x_off
                    } else {
                        x.left = rect.left+opt.x_off
                    }
                    x.top = rect.bottom+opt.y_off
                }

            } else {
                // Adjust to Event
                if (opt.event) {event = opt.event}
                if (event) {
                    x.left = event.pageX
                    x.top = event.pageY
                }
            }
            
            if (x.left) {
                // Check right of Screen
                if (x.left + elm.offsetWidth > pageXOffset+window.innerWidth) {
                    x.left = pageXOffset+window.innerWidth-elm.offsetWidth-10
                }
                elm.style.left = x.left + 'px'
            }
            if (x.top) {
                // Check Bottom of Screen
                if (x.top+elm.offsetHeight > pageYOffset+window.innerHeight) {
                    x.top = pageYOffset+window.innerHeight-elm.offsetHeight-10
                }
                elm.style.top = x.top + 'px'
            }
            
            
            
            // Add Background container
            if (document.querySelectorAll('.menu-background').length == 0) {
                document.body.appendChild($P.create({tag:'div',class:'menu-background',
                    onclick:'$P.menu.close()',oncontextmenu:'$P.menu.close();return false',
                }))
            }
    
        },
        
        close: function(elm_or_id) {
            let close_bkgd = 0
            if (elm_or_id === undefined) {
                $P.class.remove($P.query('.menu-container.active'),'active')
                close_bkgd = 1
            } else {
                var elm = $P.elm(elm_or_id)
                // elm.style.display='none'
                $P.class.remove(elm,'active')
                close_bkgd = ($P.query('.menu-container.active').length == 0)
            }
            
            // Remove Background
            if (close_bkgd) {
                document.querySelectorAll('.menu-background').forEach(function(bkgd) {
                    bkgd.parentNode.removeChild(bkgd)
                })
            }
        },
    },
    

    //---o:
    spinner: {
        create: function(options) {
            if (options === undefined) {options = {}}
            
            var childs = []
            for (var i =1; i<6; i++) {
                childs.push({class:'bar-pulse'+i})
                if (options.style !== undefined) {
                    childs[i-1]['style']=options.style;
                }
            }
            
            if (options.style !== undefined) {
                delete options.style
            }
            
            return $P.create(
                {class:"spinner-bar-pulse",
                    children:childs
                }, options
            )
        },
        dialog: function(options) {
            if (options === undefined){options = {}}
            var msg = options.msg
            if (msg === undefined) {msg='Loading...'}
        
            options.msg = {class:'align-c margin-t',children:[
                this.create(),
                {tag:'h3', class:'pure',id:'dlg-loading-msg',style:'opacity:0.7;',text:msg}
                ]
            }
            options.btns = []
            return $P.dialog.message(options)
        },
    },

//---Drag and Drop
    setDropEvent: function(options) {
        var elm = $P.elm(options.element)
        
        // Drop Event
        var onDrop = function(ev) {
            ev.preventDefault()
            var drag_data = ev.dataTransfer.getData("text")
            var drag_type = ev.dataTransfer.getData("dataType")
            var drop_elm = ev.target
            if (options.types === undefined || options.types.indexOf(drag_type) > -1) {
                options.onDrop(ev, drag_data, drag_type)
            }
        }
        
        var dragOver = function(ev) {
            ev.preventDefault()
            if (options.dragOver !== undefined) {
                options.dragOver(ev)
            }
        }
        
        var dragLeave = function(ev) {
            options.dragLeave(ev)
        }
        
        // Setup Listener Events
        $P.listen(elm,'drop',onDrop)
        $P.listen(elm,'dragover',dragOver)
        if (options.dragLeave !== undefined) {
            $P.listen(elm,'dragleave',dragLeave)
        }
    },
    
    setDragEvent: function(options) {
        var elm = $P.elm(options.element)
        
        if (options.attr === undefined) {options.attr='id'}
        
        var dragStart = function(ev) {
            ev.dataTransfer.setData("text", ev.target.getAttribute(options.attr))
            ev.dataTransfer.setData("dataType", options.type)
        
            if (options.dragStart !== undefined) {
                options.dragStart(ev)
            }
        }
        
        var dragEnd = function(ev) {
            options.dragEnd(ev)
        }
        
        $P.listen(elm,'dragstart',dragStart)
        if (options.dragEnd !== undefined) {
            $P.listen(elm,'dragend',dragEnd)
        }
        
    },
    
    setDraggable: function(elements) {
        $P.iterElements(elements, function(elm){
            $P.attr(elm,'draggable','true')
        })
    },


//---
//---Utilities (Some Nice JavaScript Functions)

//---String Functions
    replaceAll: function(string, search_val, replace_val) {
        return string.split(search_val).join(replace_val)
    },
    
    sanitize: function(txt,mode,replace_text) {
        if (replace_text === undefined){replace_text = ''}
        var reg = /[^a-z0-9'_\- .]/gi
        if (mode == 'file') {
            reg = /[^a-z0-9'_\- .\]\[\(\)\&]/gi
        } else if (mode == 'strict') {
            reg = /[^a-z0-9_]/gi
        } else if (mode == 'html') {
            return txt.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }
        return txt.replace(reg, replace_text)
    },
//---Array Functions
    //---o:
    array: {
        move: function(array, fromIndex, toIndex) {
            return array.splice(toIndex, 0, array.splice(fromIndex,1)[0])
        },
        remove: function(array, element) {
            for (var i = array.length-1; i >=0; i--) {
                if (array[i] == element) {
                    array.splice(i,1)
                }
            }
            return array
        },
    },

//---Number Functions 
    round:function(n,d) {
        if (d === undefined) {d=0}
        return parseFloat(n.toFixed(d))
    },
    
    random:function(n) {
        if (n === undefined) {n=1}
        return Math.round(Math.random()*n)
    },
    
    isNumber:function(n) {
        return !isNaN(parseFloat(n)) && isFinite(n)
    },

    uuid: function(dash) {
        var d = new Date().getTime()
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now() //use high-precision timer if available
        }
        let s = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
        if (dash !== undefined && !dash) {s = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'}
        return s.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0
            d = Math.floor(d / 16)
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
        })
    },

//---Date Functions
    //---o:
    date:{
        //---o:
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        weekdays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        month_day_counts: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

        date:function(year_or_datestr,month,day) {
            if (year_or_datestr === undefined) {
                return new Date()
            }
            if (month === undefined) {
                return new Date(year_or_datestr +' 00:00')
            }
            if (day === undefined) {day=1}
            return new Date(year_or_datestr+'-'+month+'-'+day+' 00:00')
        },
        
        str:function(year_or_date,month,day){
            let m,d,y
            if (year_or_date === undefined) {
                year_or_date = new Date()
            }
            if (month !== undefined) {
                y = year_or_date
                m = month//-1
                d = day
                if (d === undefined) {d = 1}
            } else {
                m = year_or_date.getMonth()+1
                d= year_or_date.getDate()
                y = year_or_date.getFullYear()
            }
            if (m < 10) {
                m = '0'+m
            }
            if (d < 10) {
                d = '0'+d
            }
            return ''+y+'-'+m+'-'+d
            
        },

    },

//---Other
    merge: function(obj1,obj2) {
        var obj3 = {}
        if (typeof Object.assign !== 'function') {
            for (var atr in obj1) { obj3[atr] = obj1[atr]}
            for (var atr in obj2) { obj3[atr] = obj2[atr]}
        } else {
            obj3 = Object.assign({}, obj1, obj2)
        }
        return obj3
    },
    
    //---f:
    fetch: async function(url, typeOrOptions, options) {
        let opt = {}
        let resp_type
        if (typeof(typeOrOptions) == 'string') {
            resp_type = typeOrOptions
            opt = options
        } else {
            opt = typeOrOptions
            if (typeof(options) == 'string') {
                resp_type = options
            }
        }
        
        let resp = await fetch(url,opt)
        
        // Check for errors
        if (!resp.ok) {
            throw new Error(`Error fetching: ${resp.status}`)
        }
        
        // Return response type or response
        if (resp_type) {
            let output = await resp[resp_type]()
            return output
        } else {
            return resp
        }
        
    },
    
    ajax: function(ajaxD) {
        var tempD = {
            method:'GET',
            // contentType: 'application/json;charset=UTF-8',
            // responseType:'',
            // request:'',
            'async':1,
        }
        var aD = $P.merge(tempD,ajaxD)
        
        var xhr_doneP = {}
        var promise_ok = 0
        if ('Promise' in window) {
            xhr_doneP = $P.promise()
            promise_ok = 1
        }
        
        var xhr = new XMLHttpRequest()
        xhr_doneP.xhr = xhr
        xhr.open(aD.method, aD.url, aD.async)
        if (aD.responseType != '' && aD.responseType !== undefined) {
            xhr.responseType = aD.responseType
        }
        
        // Listening Events
        if (aD.events) {
            aD.events.forEach(function(evt){
                if (evt.obj) {
                    xhr[evt.obj].addEventListener(evt.event,evt.func,false)
                } else {
                    xhr.addEventListener(evt.event,evt.func,false)
                }
            })
        }
        
        xhr.onreadystatechange = function(){
            if (xhr.readyState == XMLHttpRequest.DONE ) {
                // if (xhr.readyState == 4 && xhr.status == 200) {
                if (xhr.status == 200) {
                    if (aD.success !== undefined) {
                        aD.success(xhr.response)
                    }
                    if (promise_ok) {
                        xhr_doneP.accept(xhr.response)
                    }
                } else {
                    if (aD.error != undefined) {
                        aD.error(xhr)
                    }
                    if (promise_ok) {
                        xhr_doneP.cancel(xhr)
                    }
                }
            }
        }

        if (aD.contentType != undefined) {
            xhr.setRequestHeader('Content-Type', aD.contentType)
        }
        // Add Headers
        if (aD.headers != undefined) {
            for (var ky in aD.headers) {
                xhr.setRequestHeader(ky, aD.headers[ky])
            }
        }
        if (aD.data != undefined) {
            xhr.send(aD.data)
        } else {
            xhr.send()
        }
        
        return xhr_doneP
        
    },

    copyToClipboard: function(text) {
        let elm = document.createElement('textarea')
        elm.value = text
        document.body.appendChild(elm)
        elm.select()
        document.execCommand('copy')
        document.body.removeChild(elm)
    },

    isObject: function(obj) {
        return obj === Object(obj)
    },

    //---o:
    file:{
        // File Reader with promise
        read:function(read_func,data){
            let prom = new $P.promise()
            let reader = new FileReader();
            reader.onload = function(event) {
                prom.accept(event.target.result)
            }
            reader[read_func](data)
            return prom
        },
    },
    
}
